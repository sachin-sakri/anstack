import React from 'react';
import './App.css';
import Search from './components/Search';
import NavBar from './components/NavBar';
function App() {
	return (
		<div>
			<NavBar />
			<Search />
		</div>
	);
}

export default App;
