import React, { useState } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { ListGroup, ListGroupItem } from 'reactstrap';
import uuid from 'react-uuid';
import { Link } from 'react-router-dom';
import axios from 'axios';

function Search() {
	const [url, setUrl] = useState([]);
	const [meta, setMeta] = useState([]);
	const [dns, setDns] = useState([]);

	const getMeta = () => {
		console.log(url);
		axios.get('/metatag/' + url).then((response) => {
			console.log(response.data);
			setDns([]);
			setMeta(response.data);
		});
	};

	const getDNS = () => {
		console.log(url);
		axios.get('/lookup/' + url).then((response) => {
			console.log(...response.data);
			setMeta([]);
			setDns(...response.data);
		});
	};

	const handleChange = (event) => {
		setUrl(event.target.value);
	};
	return (
		<div>
			<Form>
				<FormGroup>
					<Label sm={2}>Give an URL</Label>
					<Col sm={10}>
						<Input
							onChange={handleChange}
							type='text'
							name='url'
							placeholder='URL: Do not include http or https.'
						/>
					</Col>
				</FormGroup>
				<Link to={`/metatag/${url}`}>
					<Button className='ml-3' onClick={getMeta}>
						Get MetaTag
					</Button>
				</Link>
				<Link to={`/lookup/${url}`}>
					<Button className='ml-3' onClick={getDNS}>
						Get DNS Lookup
					</Button>
				</Link>
			</Form>
			<ListGroup className='mt-4'>
				<h4>Data will be rendered here.</h4>
				{Object.entries(meta).map(([key, value]) => {
					return (
						<ListGroupItem key={uuid()} color='warning'>
							{key.toUpperCase() + '      '}:{'     ' + value}
						</ListGroupItem>
					);
				})}
				{Object.entries(dns).map(([key, value]) => {
					return (
						<ListGroupItem key={uuid()} color='warning'>
							{key.toUpperCase() + '   '}:{'   ' + value}
						</ListGroupItem>
					);
				})}
			</ListGroup>
		</div>
	);
}

export default Search;
