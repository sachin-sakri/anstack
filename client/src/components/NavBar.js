import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';
function NavBar() {
	return (
		<div>
			<Navbar color='dark' dark>
				<NavbarBrand href='/'>Home</NavbarBrand>
			</Navbar>
		</div>
	);
}

export default NavBar;
