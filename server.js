const express = require('express');
const router = express();
const metaget = require('metaget');
const dnsRecords = require('@layered/dns-records');
const port = process.env.PORT || 8000;
const path = require('path');

if (process.env.NODE_ENV === 'production') {
	// set static folder where react:build builds
	router.use(express.static('client/build'));
	router.get('/*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

router.get('/metatag/:url', async (req, res) => {
	console.log('https://www.' + req.params.url);
	const getData = async () => {
		try {
			let metaRespone = await metaget.fetch('https://www.' + req.params.url);
			console.log(metaRespone);
			return metaRespone;
		} catch (ex) {
			console.log('Failed', ex);
		}
	};
	res.json(await getData());
});

router.get('/lookup/:url', async (req, res) => {
	console.log(req.params.url);
	const lookUp = async () => {
		const txtRecords = await dnsRecords.getDnsRecords(req.params.url, 'TXT');
		console.log(txtRecords);
		return txtRecords;
	};
	res.json(await lookUp());
});

// This could handle using built-ins. But less efficient for accessing all data.
// router.get('/ls', async (req, res) => {
// 	test = () =>
// 		dns.resolveTxt('antstack.io', (err, records) => {
// 			return records;
// 		});
// 	res.send(await test());
// });

router.listen(port, console.log(`Server started at port ${port}`));
