# Assignment : Accsessing the Metatag data and DNS Lookup TXT

I have used React in Frontend, using functional components and hooks for state. I have used NodeJS and ExpressJS in backend along with two npm packages.

```bash
    First Package : "@layered/dns-records": "^1.4.0"
    This has been used to get dns-records of given url. [Details](https://www.npmjs.com/package/@layered/dns-records)
```

```bash
    Second Package : "metaget": "^1.0.7"
    This has been used to get Metatag Data of given url. [Details](https://www.npmjs.com/package/metaget)
```

## Installation

Everything is running on npm. You require NPM on your local machine to run this project.

```bash
    git clone https://sachin-sakri@bitbucket.org/sachin-sakri/anstack.git
    cd antstack
    cd client
    npm install
    cd ..
    npm install
```

This will install all dependencies, now you need to run the frontend and backend respectively.

```bash
    ./anstack
    cd client
    npm run start
    open new tab on terminal
    cd ..
    nodemon server.js
```

This should start React server at port `3000` and Node server at `8000`.

### Proxy for React File

I have used proxy and set it to `http://localhost:8000` allowing connection between Frontend and Backend.
